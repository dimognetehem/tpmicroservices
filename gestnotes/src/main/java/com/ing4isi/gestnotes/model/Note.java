package com.ing4isi.gestnotes.model;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Getter
@Setter
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idNote")
    private Long idNote;

    private double valeurNote;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "idMat", nullable = false)
    private Matiere matiere;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "idEtud", nullable = false)
    private EtudiantInscrit etudiantInscrit;


}
