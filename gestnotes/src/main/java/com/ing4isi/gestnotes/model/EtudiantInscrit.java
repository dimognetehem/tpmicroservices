package com.ing4isi.gestnotes.model;

import com.ing4isi.gestnotes.model.enums.Classe;
import com.ing4isi.gestnotes.model.enums.Sexe;
import javax.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Getter
@Setter
public class EtudiantInscrit {

    @Id
    @Column(name = "idEtud")
    private Long idEtud;

    @Column(unique = true, nullable = false)
    private String matricule;

    private String nomEtud;

    private String prenomEtud;

    private Sexe sexe;

    private String email;

    private Classe classe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    private String telephone;


}
