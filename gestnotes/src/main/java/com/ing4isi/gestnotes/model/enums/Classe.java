package com.ing4isi.gestnotes.model.enums;

public enum Classe {
    ING1,
    ING2,
    ING3ISI,
    ING3SRT,
    ING4ISI,
    ING4SRT,
    ING5ISI,
    ING5SRT,
    LIC1,
    LIC2,
    LIC3,
    MASTER1,
    MASTER2

}
