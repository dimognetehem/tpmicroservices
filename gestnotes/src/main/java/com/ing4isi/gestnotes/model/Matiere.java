package com.ing4isi.gestnotes.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Getter
@Setter
public class Matiere {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idMat")
    private Long idMat;

    private String nomMat;

    private double credit;
    
    private int semestre;


}
