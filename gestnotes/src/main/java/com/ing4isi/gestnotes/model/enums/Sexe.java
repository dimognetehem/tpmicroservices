package com.ing4isi.gestnotes.model.enums;

public enum Sexe {
    MASCULIN,
    FEMININ
}
