package com.ing4isi.gestnotes.controller;


import com.ing4isi.gestnotes.model.EtudiantInscrit;
import com.ing4isi.gestnotes.model.Note;
import com.ing4isi.gestnotes.repository.NoteRepository;
import com.ing4isi.gestnotes.service.EtudiantInscritService;
import com.ing4isi.gestnotes.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/notes")
public class NoteController {

    @Autowired
    private NoteService noteServ;



//    @GetMapping("/list/{id}")
//    public List<Note> NoteListByEtud(@PathVariable("id") Long id) {
//
//        return noteServ.listNotesByEtud(id);
//    }

//    @GetMapping("/listetud")
//    public List<EtudiantInscrit> ListeEtuds(){
//        return etudServ.listAll();
//    }

    @GetMapping("/listnote/{id}")
    public List<Note> NoteListeParEtud(@PathVariable("id") Long id){
        return noteServ.listNotesByEtud(id);
    }

    @GetMapping("/listnotesem1/{id}")
    public List<Note> NoteListeParEtudSem1(@PathVariable("id") Long id){
        return noteServ.listNotesByEtudSem1(id);
    }

    @GetMapping("/listnotesem2/{id}")
    public List<Note> NoteListeParEtudSem2(@PathVariable("id") Long id){
        return noteServ.listNotesByEtudSem2(id);
    }



}
