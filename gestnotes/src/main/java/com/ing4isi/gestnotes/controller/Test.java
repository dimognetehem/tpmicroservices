package com.ing4isi.gestnotes.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notes")
public class Test {

    @GetMapping("/test")
    public String test(){
        return "test successful";
    }
}
