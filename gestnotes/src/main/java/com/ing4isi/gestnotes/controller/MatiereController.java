package com.ing4isi.gestnotes.controller;


import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.service.MatiereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notes")
public class MatiereController {

    @Autowired
    private MatiereService matServ;

    @PostMapping("/saveMat")
    /* Le @RequestBody a permis de récupérer les données fournies par le client UI avant l'enregistrement en BD */
    public void saveMatiere(@RequestBody Matiere matiere) {

        System.out.println(matiere);
        matServ.save(matiere);

    }

    @GetMapping("/matieres")
    public List<Matiere> listMat(){
        return matServ.listAll();

    }


}
