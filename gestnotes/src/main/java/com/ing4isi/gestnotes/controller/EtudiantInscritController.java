package com.ing4isi.gestnotes.controller;


import com.ing4isi.gestnotes.model.EtudiantInscrit;
import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.service.EtudiantInscritService;
import com.ing4isi.gestnotes.service.MatiereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notes")
public class EtudiantInscritController {

    @Autowired
    private EtudiantInscritService etudInscritServ;

    @GetMapping("/listetud")
    public List<EtudiantInscrit> testlist(){

        return etudInscritServ.listAll();
    }

    @PostMapping("/saveEtudIns")
    /* Le @RequestBody a permis de récupérer les données fournies par le client UI avant l'enregistrement en BD */
    public void saveEtudIns(@RequestBody EtudiantInscrit etudiantInscrit) {

        System.out.println(etudiantInscrit);
//        etudInscritServ.delete(etudiantInscrit);

        etudInscritServ.save(etudiantInscrit);

    }

    @DeleteMapping("/deleteAllEtudIns")
    public String deleteAllEtudIns() {
        etudInscritServ.suppressAll();

        return "Table EtudiantInscrit a été vidée";
    }


}
