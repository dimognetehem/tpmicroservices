package com.ing4isi.gestnotes.repository;

import com.ing4isi.gestnotes.model.EtudiantInscrit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EtudiantInscritRepository extends JpaRepository<EtudiantInscrit, Long> {

    @Modifying
    @Query(value = "DELETE FROM EtudiantInscrit e")
    void supprimerAllEtudIns();
}
