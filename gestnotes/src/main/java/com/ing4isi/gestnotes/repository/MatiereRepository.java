package com.ing4isi.gestnotes.repository;

import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MatiereRepository extends JpaRepository<Matiere, Long> {



    @Query(value = "SELECT m.nomMat FROM Matiere m")
    List<String> getAllMats();
}
