package com.ing4isi.gestnotes.repository;

import com.ing4isi.gestnotes.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

//    List<Note> getNotesByEtudiantInscrit(Long idEtud);

    @Query(value = "SELECT n FROM Note n WHERE n.etudiantInscrit.idEtud = ?1")
    List<Note> recupNotesEtud(Long idEtud);

    @Query(value = "SELECT n FROM Note n WHERE n.etudiantInscrit.idEtud = ?1 AND n.matiere.semestre = 1")
    List<Note> recupNotesEtudParSem1(Long idEtud);

    @Query(value = "SELECT n FROM Note n WHERE n.etudiantInscrit.idEtud = ?1 AND n.matiere.semestre = 2")
    List<Note> recupNotesEtudParSem2(Long idEtud);
}
