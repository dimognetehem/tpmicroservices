package com.ing4isi.gestnotes.service;


import com.ing4isi.gestnotes.model.EtudiantInscrit;
import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.repository.EtudiantInscritRepository;
import com.ing4isi.gestnotes.repository.MatiereRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EtudiantInscritService {

    @Autowired
    private EtudiantInscritRepository etudInscritRepo;

    public List<EtudiantInscrit> listAll() {
        return (List<EtudiantInscrit>) etudInscritRepo.findAll();
    }

    public void save(EtudiantInscrit etudiantInscrit) {
        etudInscritRepo.save(etudiantInscrit);
    }

    public void delete(EtudiantInscrit etudiantInscrit) {
        etudInscritRepo.delete(etudiantInscrit);
    }

    public void suppressAll(){ etudInscritRepo.deleteAll();}
}
