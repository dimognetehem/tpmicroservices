package com.ing4isi.gestnotes.service;


import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.repository.MatiereRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MatiereService {

    @Autowired
    private MatiereRepository matiereRepo;

    public List<Matiere> listAll() {
        return (List<Matiere>) matiereRepo.findAll();
    }

    public void save(Matiere matiere) {
        matiereRepo.save(matiere);
    }

    public Matiere get(Long id){
        Optional<Matiere> result = matiereRepo.findById(id);
        if (result.isPresent()) {
            return result.get();
        }

        return null;
    }

    public void delete(Long id){
        matiereRepo.deleteById(id);
    }

    public List<String> testlistMat(){ return matiereRepo.getAllMats();}
}
