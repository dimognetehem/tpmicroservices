package com.ing4isi.gestnotes.service;


import com.ing4isi.gestnotes.model.Matiere;
import com.ing4isi.gestnotes.model.Note;
import com.ing4isi.gestnotes.repository.MatiereRepository;
import com.ing4isi.gestnotes.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepo;

    public List<Note> listAll() {
        return (List<Note>) noteRepo.findAll();
    }

    public List<Note> listNotesByEtud(Long idEtud){ return noteRepo.recupNotesEtud(idEtud); }

    public List<Note> listNotesByEtudSem1(Long idEtud){ return noteRepo.recupNotesEtudParSem1(idEtud); }

    public List<Note> listNotesByEtudSem2(Long idEtud){ return noteRepo.recupNotesEtudParSem2(idEtud); }

}
