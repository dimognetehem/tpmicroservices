package com.ing4isi.gestincription.model;

import com.ing4isi.gestincription.model.enums.Statut;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "inscription")
@Getter
@Setter
public class Inscription {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "idIns")
  private Long idIns;

  @Column(nullable = false)
  private Statut statutIns;

  @Column(nullable = false)
  private String email;

  @Column(nullable = false)
  private String matricule;

  @Column(nullable = false)
  private float montant;


}
