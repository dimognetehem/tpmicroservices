package com.ing4isi.gestincription.model;



import com.ing4isi.gestincription.model.enums.Classe;
import com.ing4isi.gestincription.model.enums.Sexe;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Candidat {


    private Long idCan;

    private String matricule;

    private String nomCan;

    private String prenomCan;

    private Sexe sexe;

    private String email;

    private Classe classe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    private Date dateCreation;

    private String telephone;

}
