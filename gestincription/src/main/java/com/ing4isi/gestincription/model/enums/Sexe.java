package com.ing4isi.gestincription.model.enums;

public enum Sexe {
    MASCULIN,
    FEMININ
}
