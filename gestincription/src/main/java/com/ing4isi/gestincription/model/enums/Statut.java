package com.ing4isi.gestincription.model.enums;

public enum Statut {
  PAYE,
  INSOLVABLE
}
