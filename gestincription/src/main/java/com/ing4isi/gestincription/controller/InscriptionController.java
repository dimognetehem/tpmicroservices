package com.ing4isi.gestincription.controller;


import com.ing4isi.gestincription.model.Inscription;
import com.ing4isi.gestincription.model.Candidat;
import com.ing4isi.gestincription.model.enums.Statut;
import com.ing4isi.gestincription.repository.InscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.Year;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping("/inscrip")
public class InscriptionController {

  @Autowired
  private InscriptionRepository inscriptionRepository;

  @Autowired
  private RestTemplate restTemplate;

  public final String GESTETUD_URL = "http://localhost:8771/gestetud/etud";


  public static final String GESTNOTIF_URL = "http://localhost:8771/gestnotif/notif";

  public static String generateMatricule() {
    int currentYear = Year.now().getValue();
    String randomString = UUID.randomUUID().toString().replace("-", "").substring(0, 3);
    return currentYear + "i" + randomString;
  }

  @GetMapping("all")
  public List<Inscription> allInscription(){
    return inscriptionRepository.findAll();
  }

  @PostMapping("inscrire/save")
  public void createInscription(@RequestBody Inscription inscription){
    System.out.println(inscription);

    Candidat candidat1 =new Candidat();
    inscription.setMatricule(generateMatricule());
    if (inscription.getMontant()<1000000) {
      inscription.setStatutIns(Statut.INSOLVABLE);
    }
    else{
      inscription.setStatutIns(Statut.PAYE);
    }

    inscriptionRepository.save(inscription);

//    emailService.sendEmail(etudiant.getMail_etud(), infos, "Informations de connexion application de gestion des interventions");
    List<Candidat> listCandidats = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTETUD_URL+"/candidats", Candidat[].class)));
      System.out.println(listCandidats);
    for (Candidat candidat : listCandidats
         ) {
      if (candidat.getEmail().equals(inscription.getEmail()))
      {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        candidat.setMatricule(inscription.getMatricule());
        HttpEntity<Candidat> entity = new HttpEntity<Candidat>(candidat,headers);
        String infos = "Cher(e) "+ candidat.getNomCan()+ " " +candidat.getPrenomCan()+",\n" +
                "Vous faites désormais partie de l'Université Saint Jean. Votre Matricule est  : " + inscription.getMatricule();
//        String infos = "Chère " + candidat.getPrenomCan() + " " + candidat.getNomCan() + " Vous faites désormais partie de l'Université Saint Jean "+ "    "+"   "+ " | Votre Matricule est  : " + inscription.getMatricule();
//        callSendEmailEndpoint(inscription.getMatricule(),infos,"Message de Bienvenue");


        restTemplate.getForObject(GESTNOTIF_URL+"/send/"+inscription.getEmail()+"/"+infos+"/"+"Inscription Réussie",Void.class);
        restTemplate.exchange(
          GESTETUD_URL+"/candidat/save", HttpMethod.POST, entity, String.class).getBody();
             }

    }

    System.out.println("inscription reussie");

  }

}
