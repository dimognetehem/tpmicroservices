package com.ing4isi.gestincription.repository;

import com.ing4isi.gestincription.model.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
}
