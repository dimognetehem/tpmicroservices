package com.ing4isi.gestnotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class GestnotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestnotificationApplication.class, args);
	}

}
