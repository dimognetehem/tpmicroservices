package com.ing4isi.gestnotification.controller;

import com.ing4isi.gestnotification.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notif")
public class EmailController {

    @Autowired
    private EmailService emailServ;


    @GetMapping("/mail")
    public String testmail(){

        emailServ.sendEmail("dimognetehem@gmail.com", "Test Réussi", "Test Mail via Gateway");

        return "email envoyé";
    }

    @GetMapping("/send/{mail}/{body}/{subject}")
    public void sendEmail(@PathVariable("mail") String toEmail, @PathVariable("body") String body, @PathVariable("subject") String subject){

        emailServ.sendEmail(toEmail, body, subject);

    }

}
