package com.ing4isi.clientui.restclient.microcomposite;

import com.ing4isi.clientui.entities.EtudInscrit;
import com.ing4isi.clientui.entities.Inscription;
import com.ing4isi.clientui.entities.Matiere;
import com.ing4isi.clientui.entities.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
public class CompositeRest {

    @Autowired
    private RestTemplate restTemplate;

    public final String COMPOSITE_URL = "http://localhost:8771/microcompo/compo";

    @GetMapping("/listinscrits")
    public ModelAndView ListInscrits(Model model) {

        List<EtudInscrit> listInscrits = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listetudinscrits", EtudInscrit[].class)));
        System.out.println(listInscrits);

        model.addAttribute("listInscrits", listInscrits);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("gestinscription/listinscrits");
        return modelAndView;
    }

    @GetMapping("/listetud")
    public ModelAndView ListNoteEtudInscrits(Model model) {

        List<EtudInscrit> listEtud = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listetudinscrits", EtudInscrit[].class)));
        System.out.println(listEtud);

        model.addAttribute("listEtud", listEtud);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("gestnotes/listetudinscrits");
        return modelAndView;
    }

    @GetMapping("/saveNote")
    public ModelAndView showNoteSavePage(Model model) {

//        List<Note> listNotes = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listnote", Note[].class)));
//        System.out.println(listMatieres);

//        model.addAttribute("matiere", new Matiere());
//        model.addAttribute("listMatieres", listMatieres);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("gestnotes/savenotes");
        return modelAndView;
    }

    @GetMapping("/listnote/{id}")
    public ModelAndView ListNotes(@PathVariable("id") Long id, Model model, RedirectAttributes ra) {

        String id1 = String.valueOf(id);

        List<Note> listNotes = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listnote/"+id1, Note[].class)));
        System.out.println(listNotes);

        if(!listNotes.isEmpty()){


            List<Note> NotesSemestre1 = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listnotesem1/"+id1, Note[].class)));
            System.out.println(NotesSemestre1);

            List<Note> NotesSemestre2 = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(COMPOSITE_URL + "/listnotesem2/"+id1, Note[].class)));
            System.out.println(NotesSemestre2);



            String nomEtud = NotesSemestre1.get(0).getEtudiantInscrit().getNomEtud();
            String prenomEtud = NotesSemestre1.get(0).getEtudiantInscrit().getPrenomEtud();
            double temp = 0;
            double temp1 = 0;
            double temp2 = 0;
            double totalcredits = 0;
            double totalcredits1 = 0;
            double totalcredits2 = 0;

            for(Note note : NotesSemestre1){
                totalcredits1 += note.getMatiere().getCredit();
                double sumpoints = note.getValeurNote() * note.getMatiere().getCredit();
                temp1+=sumpoints;
            }

            for(Note note : NotesSemestre2){
                totalcredits2 += note.getMatiere().getCredit();
                double sumpoints = note.getValeurNote() * note.getMatiere().getCredit();
                temp2+=sumpoints;
            }

            for(Note note : listNotes){
                totalcredits += note.getMatiere().getCredit();
                double sumpoints = note.getValeurNote() * note.getMatiere().getCredit();
                temp+=sumpoints;
            }

            double moysem1 = Math.floor(temp1/totalcredits1);
            double moysem2 = Math.floor(temp2/totalcredits2);
            double moygen = (moysem1+moysem2)/2;

            model.addAttribute("moyenne", moygen);
            model.addAttribute("moysem1", moysem1);
            model.addAttribute("moysem2", moysem2);
            model.addAttribute("nomEtud", nomEtud);
            model.addAttribute("prenomEtud", prenomEtud);

            model.addAttribute("notesem1", NotesSemestre1);
            model.addAttribute("notesem2", NotesSemestre2);
            model.addAttribute("pageTitle", "Edit User (ID: " + id + ")");

            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("gestnotes/listnotes");

            return modelAndView;

        }
        else {
            return new ModelAndView("redirect:/listetud");
        }

    }


}
