//package com.ing4isi.clientui.restclient;
//
//
//import com.ing4isi.clientui.entities.Candidat;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.*;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Objects;
//
//@RestController
//public class RestClient {
//
//    @Autowired
//    private RestTemplate restTemplate;
//
//    public final String GESTETUD_URL = "http://localhost:8771/gestetud/etud";
//
//
//    @GetMapping("")
//    public ModelAndView index () {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("index");
//        return modelAndView;
//    }
//
//    @GetMapping("/candidats")
//    public ModelAndView showCandidatList(Model entities) {
//
//        List<Candidat> listCandidats = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTETUD_URL + "/candidats", Candidat[].class)));
//        System.out.println(listCandidats);
//
//        entities.addAttribute("listCandidats", listCandidats);
//
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("gestetud/listcandidats");
//        return modelAndView;
//    }
//
//    @GetMapping("/candidats/new")
//    public ModelAndView showNewForm(Model entities) {
//        entities.addAttribute("candidat", new Candidat());
//        entities.addAttribute("pageTitle", "Add New User");
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("gestetud/savecandidats");
//        return modelAndView;
//    }
//
//    @PostMapping("/candidat/save")
//    public ModelAndView createCandidat(Candidat candidat, RedirectAttributes ra) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        HttpEntity<Candidat> entity = new HttpEntity<Candidat>(candidat,headers);
//
//        restTemplate.exchange(
//                GESTETUD_URL+"/candidat/save", HttpMethod.POST, entity, String.class).getBody();
//        ra.addFlashAttribute("message", "The user has been saved successfully.");
//        return new ModelAndView("redirect:/candidats");
//    }
//
//    @GetMapping("/candidat/edit/{id}")
//    public ModelAndView showEditForm(@PathVariable("id") Long id, Model entities, RedirectAttributes ra) {
//        try {
//            String id1 = String.valueOf(id);
//            Candidat candidat = restTemplate.getForObject(GESTETUD_URL + "/candidat/edit/"+id1, Candidat.class);
//            System.out.println(candidat);
//            entities.addAttribute("candidat", candidat);
//            entities.addAttribute("pageTitle", "Edit User (ID: " + id + ")");
//
//            ModelAndView modelAndView = new ModelAndView();
//            modelAndView.setViewName("gestetud/savecandidats");
//
//            return modelAndView;
//        } catch (Error e) {
//            ra.addFlashAttribute("message", e.getMessage());
//            return new ModelAndView("redirect:/candidats");
//        }
//    }
//
//    @GetMapping("/candidat/delete/{id}")
//    public ModelAndView deleteCandidat(@PathVariable("id") Long id, Model entities, RedirectAttributes ra) {
//        String id1 = String.valueOf(id);
//        restTemplate.getForObject(GESTETUD_URL + "/candidat/delete/"+id1, Void.class);
//        ra.addFlashAttribute("message", "The user ID " + id + " has been deleted.");
//        return new ModelAndView("redirect:/candidats");
//    }
//
//
//
////    public void saveCandidat(HttpServletResponse resp, Candidat candidat, RedirectAttributes ra) throws IOException {
////        System.out.println("Tessssssssssssssssssssssssssst");
////        System.out.println(candidat);
////        HttpHeaders headers = new HttpHeaders();
////        headers.setContentType(MediaType.APPLICATION_JSON);
////
////        HttpEntity<Candidat> request = new HttpEntity<>(candidat, headers);
////
////
////        try {
////            ResponseEntity<Void> response = restTemplate.postForEntity(GESTETUD_URL + "/candidat/save", request, Void.class);
////            ra.addFlashAttribute("message", "The candidat has been saved successfully.");
////        } catch (Exception e) {
////            // Handle exceptions if necessary
////            ra.addFlashAttribute("message", "Failed to save the candidat: " + e.getMessage());
////        }
////
////        resp.sendRedirect("http://localhost:5001/candidats");
//////        return "redirect:/http://localhost:5001/candidats";
////
////    }
//
//
//
//}
