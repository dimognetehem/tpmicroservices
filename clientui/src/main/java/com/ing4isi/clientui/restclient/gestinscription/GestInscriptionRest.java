package com.ing4isi.clientui.restclient.gestinscription;


import com.ing4isi.clientui.entities.Candidat;
import com.ing4isi.clientui.entities.Inscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
public class GestInscriptionRest {

    @Autowired
    private RestTemplate restTemplate;

    public final String GESTINSCRIP_URL = "http://localhost:8771/gestinscription/inscrip";

    public final String GESTETUD_URL = "http://localhost:8771/gestetud/etud";

//    @GetMapping("/listinscrits")
//    public ModelAndView ListInscrits(Model model) {
//
//        List<Inscription> listInscrits = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTINSCRIP_URL + "/all", Inscription[].class)));
//        System.out.println(listInscrits);
//
//        model.addAttribute("listInscrits", listInscrits);
//
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("gestinscription/listinscrits");
//        return modelAndView;
//    }

    @GetMapping("/inscrire")
    public ModelAndView showInscriptionForm(Model model) {
        model.addAttribute("inscription", new Inscription());
        model.addAttribute("pageTitle", "Add New Inscrit");

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("gestinscription/inscrire");
        return modelAndView;
    }

    @PostMapping("/inscrire/save")
    public ModelAndView Inscrire(Inscription inscription, RedirectAttributes ra, Model model) {

        try{
            boolean ispresent = false;

            System.out.println(inscription);
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<Inscription> entity = new HttpEntity<Inscription>(inscription,headers);
            List<Candidat> listCandidats = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTETUD_URL+"/candidats", Candidat[].class)));

            for (Candidat candidat : listCandidats
            ) {
                if (candidat.getEmail().equals(inscription.getEmail())) {
                    ispresent = true;
                    break;
                }

            }
            if(ispresent){
                restTemplate.exchange(
                        GESTINSCRIP_URL+"/inscrire/save", HttpMethod.POST, entity, String.class).getBody();
                ra.addFlashAttribute("message1", "Etudiant inscrit avec succès");

                return new ModelAndView("redirect:/listinscrits");
            }
            else {
                ra.addFlashAttribute("message", "L'etudiant que vous voulez inscrire n'existe pas");

                return new ModelAndView("redirect:/inscrire");
            }

        }
        catch (Error e){
            System.out.println(e.getMessage());
        }

        return null;
    }


}
