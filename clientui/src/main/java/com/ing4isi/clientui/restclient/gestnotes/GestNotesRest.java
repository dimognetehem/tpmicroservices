package com.ing4isi.clientui.restclient.gestnotes;

import com.ing4isi.clientui.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


@RestController
public class GestNotesRest {

    @Autowired
    private RestTemplate restTemplate;

    public final String GESTNOTE_URL = "http://localhost:8771/gestnotes/notes";

//    @GetMapping("/listetud")
//    public ModelAndView ListEtudInscrits(Model model) {
//
//        List<EtudiantInscrit> listEtud = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listetud", EtudiantInscrit[].class)));
//        System.out.println(listEtud);
//
//        model.addAttribute("listEtud", listEtud);
//
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("gestnotes/listetudinscrits");
//        return modelAndView;
//    }

    @GetMapping("/matieres")
    public ModelAndView showMatiereList(Model model) {

        List<Matiere> listMatieres = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/matieres", Matiere[].class)));
        System.out.println(listMatieres);

        model.addAttribute("matiere", new Matiere());
        model.addAttribute("listMatieres", listMatieres);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("gestnotes/savematieres");
        return modelAndView;
    }

    @PostMapping("/saveMat")
    public ModelAndView createMat(Matiere matiere, RedirectAttributes ra) {
        System.out.println(matiere);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Matiere> entity = new HttpEntity<Matiere>(matiere,headers);

        restTemplate.exchange(
                GESTNOTE_URL+"/saveMat", HttpMethod.POST, entity, String.class).getBody();
        ra.addFlashAttribute("message", "The user has been saved successfully.");
        return new ModelAndView("redirect:/matieres");
    }




//    @GetMapping("/addnote/{id}")
//    public ModelAndView AddNotes(@PathVariable("id") Long id, Model model, RedirectAttributes ra) {
//
//        String id1 = String.valueOf(id);
//
//        List<Note> listNotes = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnote/"+id1, Note[].class)));
//        System.out.println(listNotes);
//
//        if(!listNotes.isEmpty()){
//
//
//            List<Note> NotesSemestre1 = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnotesem1/"+id1, Note[].class)));
//            System.out.println(NotesSemestre1);
//
//            List<Note> NotesSemestre2 = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnotesem2/"+id1, Note[].class)));
//            System.out.println(NotesSemestre2);
//
//
//
//
//
//            String nomEtud = listNotes.get(0).getEtudiantInscrit().getNomEtud();
//            String prenomEtud = listNotes.get(0).getEtudiantInscrit().getPrenomEtud();
//
//
//            model.addAttribute("nomEtud", nomEtud);
//            model.addAttribute("prenomEtud", prenomEtud);
//
//            model.addAttribute("notesem1", NotesSemestre1);
//            model.addAttribute("notesem2", NotesSemestre2);
//            model.addAttribute("pageTitle", "Edit User (ID: " + id + ")");
//
//            ModelAndView modelAndView = new ModelAndView();
//            modelAndView.setViewName("gestnotes/listnotes");
//
//            return modelAndView;
//
//        }
//        else {
//            return new ModelAndView("redirect:/listetud");
//        }
//
//    }

}
