package com.ing4isi.clientui.entities.enums;

public enum Sexe {
    MASCULIN,
    FEMININ
}
