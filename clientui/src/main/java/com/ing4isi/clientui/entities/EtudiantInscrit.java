package com.ing4isi.clientui.entities;

import com.ing4isi.clientui.entities.enums.Classe;
import com.ing4isi.clientui.entities.enums.Sexe;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EtudiantInscrit {

    private Long idEtud;

    private String matricule;

    private String nomEtud;

    private String prenomEtud;

    private Sexe sexe;

    private String email;

    private Classe classe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    private Date dateCreation;

    private String telephone;


}
