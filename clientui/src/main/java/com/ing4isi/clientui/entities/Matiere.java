package com.ing4isi.clientui.entities;


import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Matiere {

    private Long idMat;

    private String nomMat;

    private double credit;
    
    private int semestre;

}
