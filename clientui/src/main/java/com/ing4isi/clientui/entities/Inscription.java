package com.ing4isi.clientui.entities;

import com.ing4isi.clientui.entities.enums.Statut;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Inscription {

  private Long idIns;

  private Statut statutIns;

  private String email;

  private String matricule;

  private float montant;

}
