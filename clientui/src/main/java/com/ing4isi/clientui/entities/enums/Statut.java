package com.ing4isi.clientui.entities.enums;

public enum Statut {
  PAYE,
  INSOLVABLE
}
