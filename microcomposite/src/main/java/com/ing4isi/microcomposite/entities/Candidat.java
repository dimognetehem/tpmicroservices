package com.ing4isi.microcomposite.entities;


import com.ing4isi.microcomposite.entities.enums.Classe;
import com.ing4isi.microcomposite.entities.enums.Sexe;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Candidat {


    private Long idCan;

    private String matricule;

    private String nomCan;

    private String prenomCan;

    private Sexe sexe;

    private String email;

    private Classe classe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    private Date dateCreation;

    private String telephone;

}
