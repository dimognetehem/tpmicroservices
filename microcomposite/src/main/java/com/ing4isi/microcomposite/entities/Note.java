package com.ing4isi.microcomposite.entities;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Note {

    private Long idNote;

    private double valeurNote;

    private Matiere matiere;

    private EtudiantInscrit etudiantInscrit;

}
