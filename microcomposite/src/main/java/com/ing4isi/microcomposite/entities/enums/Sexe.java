package com.ing4isi.microcomposite.entities.enums;

public enum Sexe {
    MASCULIN,
    FEMININ
}
