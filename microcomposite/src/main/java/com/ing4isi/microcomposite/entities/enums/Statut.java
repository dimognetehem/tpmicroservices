package com.ing4isi.microcomposite.entities.enums;

public enum Statut {
  PAYE,
  INSOLVABLE
}
