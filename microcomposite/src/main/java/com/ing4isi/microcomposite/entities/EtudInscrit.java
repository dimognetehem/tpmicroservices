package com.ing4isi.microcomposite.entities;

import com.ing4isi.microcomposite.entities.enums.Classe;
import com.ing4isi.microcomposite.entities.enums.Sexe;
import com.ing4isi.microcomposite.entities.enums.Statut;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EtudInscrit {

    private Long idEtud;

    private String matricule;

    private String nomEtudIns;

    private String prenomEtudIns;

    private Sexe sexe;

    private String email;

    private Classe classe;

    private float montant;

    private Statut statutIns;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    private Date dateCreation;

    private String telephone;

}
