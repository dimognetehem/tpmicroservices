package com.ing4isi.microcomposite.controller;


import com.ing4isi.microcomposite.entities.Candidat;
import com.ing4isi.microcomposite.entities.EtudInscrit;
import com.ing4isi.microcomposite.entities.Inscription;
import com.ing4isi.microcomposite.entities.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@RestController
@RequestMapping("/compo")
public class EtudInscritController {

    @Autowired
    private RestTemplate restTemplate;

    public final String GESTINSCRIP_URL = "http://localhost:8771/gestinscription/inscrip";

    public final String GESTETUD_URL = "http://localhost:8771/gestetud/etud";

    public final String GESTNOTE_URL = "http://localhost:8771/gestnotes/notes";

    @GetMapping("/listetudinscrits")
    public List<EtudInscrit> ListEtudInscrits(){

        List<EtudInscrit> etudiantsinscrits = new ArrayList<>();
//        List<EtudInscrit> test = new HashSet<>()
        List<Candidat> listCandidats = Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTETUD_URL+"/candidats", Candidat[].class)));
        List<Inscription> listInscriptions= Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTINSCRIP_URL+"/all", Inscription[].class)));
        EtudInscrit etudInscrit = new EtudInscrit();
        for (Candidat candidat:listCandidats) {

            if(candidat.getMatricule() != null){
                for (Inscription listInscription : listInscriptions) {

                    if (candidat.getMatricule().equals(listInscription.getMatricule())) {
                        etudInscrit = new EtudInscrit();

                        etudInscrit.setIdEtud(listInscription.getIdIns());
                        etudInscrit.setMatricule(candidat.getMatricule());
                        etudInscrit.setEmail(candidat.getEmail());
                        etudInscrit.setPrenomEtudIns(candidat.getPrenomCan());
                        etudInscrit.setNomEtudIns(candidat.getNomCan());
                        etudInscrit.setSexe(candidat.getSexe());
                        etudInscrit.setTelephone(candidat.getTelephone());
                        etudInscrit.setClasse(candidat.getClasse());
                        etudInscrit.setDateNaiss(candidat.getDateNaiss());
                        etudInscrit.setDateCreation(candidat.getDateCreation());
                        etudInscrit.setMontant(listInscription.getMontant());
                        etudInscrit.setStatutIns(listInscription.getStatutIns());


                    }

                }

                etudiantsinscrits.add(etudInscrit);

            }
        }
//        restTemplate.exchange(GESTNOTE_URL+"/deleteAllEtudIns", HttpMethod.DELETE, null, String.class);

        for (EtudInscrit etudInscrit1: etudiantsinscrits){
            for (Inscription listInscription : listInscriptions) {
                if(etudInscrit1.getMatricule().equals(listInscription.getMatricule())){

                    etudInscrit1.setIdEtud(listInscription.getIdIns());
                    
                    restTemplate.postForObject(GESTNOTE_URL+"/saveEtudIns", etudInscrit1,Void.class);
                }


            }

        }

        return etudiantsinscrits;
    }


    @GetMapping("/listnote/{id}")
    public List<Note> ListNotes(@PathVariable("id") Long id){

        String id1 = String.valueOf(id);

        return Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnote/"+id1, Note[].class)));
    }

    @GetMapping("/listnotesem1/{id}")
    public List<Note> ListNotesSem1(@PathVariable("id") Long id){

        String id1 = String.valueOf(id);

        return Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnotesem1/"+id1, Note[].class)));
    }


    @GetMapping("/listnotesem2/{id}")
    public List<Note> ListNotesSem2(@PathVariable("id") Long id){

        String id1 = String.valueOf(id);

        return Arrays.asList(Objects.requireNonNull(restTemplate.getForObject(GESTNOTE_URL + "/listnotesem2/"+id1, Note[].class)));
    }


}
