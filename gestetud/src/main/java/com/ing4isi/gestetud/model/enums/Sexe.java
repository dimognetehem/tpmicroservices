package com.ing4isi.gestetud.model.enums;

public enum Sexe {
    MASCULIN,
    FEMININ
}
