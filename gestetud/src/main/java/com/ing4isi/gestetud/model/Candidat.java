package com.ing4isi.gestetud.model;


import com.ing4isi.gestetud.model.enums.Classe;
import com.ing4isi.gestetud.model.enums.Sexe;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Getter
@Setter
public class Candidat {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idCan")
    private Long idCan;

    @Column(unique = true, nullable = true)
    private String matricule;

    private String nomCan;

    private String prenomCan;

    private Sexe sexe;

    private String email;

    private Classe classe;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNaiss;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation = new Date();

    private String telephone;

}
