package com.ing4isi.gestetud.repository;

import com.ing4isi.gestetud.model.Candidat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CandidatRepository extends JpaRepository<Candidat, Long> {
}
