package com.ing4isi.gestetud.controller;


import com.ing4isi.gestetud.model.Candidat;
import com.ing4isi.gestetud.service.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/etud")
public class CandidatController {

    @Autowired
    private CandidatService candidatServ;


    @GetMapping("/candidats")
    public List<Candidat> showCandidatList() {

        return candidatServ.listAll();
    }
//    @GetMapping("/candidats/new")
//    public String showNewForm(Model entities) {
//        entities.addAttribute("candidat", new Candidat());
//        entities.addAttribute("pageTitle", "Add New User");
//        return "user_form";
//    }

    @PostMapping("/candidat/save")
    /* Le @RequestBody a permis de récupérer les données fournies par le client UI avant l'enregistrement en BD */
    public void saveCandidat(@RequestBody Candidat candidat) {

        System.out.println(candidat);
        candidatServ.save(candidat);

//        resp.sendRedirect("http://localhost:5001/candidats");

    }

//    @GetMapping("/candidat/edit/{id}")
//    public String showEditForm(@PathVariable("id") Long id, Model entities, RedirectAttributes ra) {
//        try {
//            Candidat candidat = candidatServ.get(id);
//            entities.addAttribute("candidat", candidat);
//            entities.addAttribute("pageTitle", "Edit User (ID: " + id + ")");
//
//            return "user_form";
//        } catch (Error e) {
//            ra.addFlashAttribute("message", e.getMessage());
//            return "redirect:http://localhost:8771/gestetud/etud/candidats";
//        }
//    }
    @GetMapping("/candidat/edit/{id}")
    public Candidat showEditForm(@PathVariable("id") Long id) {
        System.out.println(id);
        System.out.println(candidatServ.get(id));
        return candidatServ.get(id);
    }

    @GetMapping("/candidat/delete/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        candidatServ.delete(id);
        System.out.println("Suppression d'un candidat");
    }


//    @GetMapping("/candidat/delete/{id}")
//    public String deleteUser(@PathVariable("id") Long id, RedirectAttributes ra) {
//        try {
//            candidatServ.delete(id);
//            ra.addFlashAttribute("message", "The user ID " + id + " has been deleted.");
//        } catch (Error e) {
//            ra.addFlashAttribute("message", e.getMessage());
//        }
//        return "redirect:http://localhost:8771/gestetud/etud";
//    }


}
