package com.ing4isi.gestetud.service;

import com.ing4isi.gestetud.model.Candidat;
import com.ing4isi.gestetud.repository.CandidatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CandidatService {

    @Autowired
    private CandidatRepository candidatRepo;


    public List<Candidat> listAll() {
        return (List<Candidat>) candidatRepo.findAll();
    }

    public void save(Candidat candidat) {
        candidatRepo.save(candidat);
    }

    public Candidat get(Long id){
        Optional<Candidat> result = candidatRepo.findById(id);
        if (result.isPresent()) {
            return result.get();
        }

        return null;
    }

    public void delete(Long id){
        candidatRepo.deleteById(id);
    }


}
